
// Matches played per year

const matchPlayed = function (matchData) {

    let matchPlayed = {};

    matchData.forEach(match => {
        year = match.season;

        if (year in matchPlayed) {
            matchPlayed[year] += 1;
        }
        else {
            matchPlayed[year] = 1;
        }
    })

    
    return {
        obj:matchPlayed,
        fileName:'matchesPerYear'

    };

}




// Number of matches won per team per year in IPL

const matchesWonPerTeamPerYear = function (matchData) {
    let teamsWon = {};
    matchData.forEach(match=>{
        team = match.team1;
        if(!(teamsWon[team])){
            teamsWon[team]={};
        }
    })

    matchData.forEach(match=>{
        year = match.season;
        wonTeam = match.winner;
        for(let team in teamsWon){
            if(!teamsWon[team][year]){
            teamsWon[team][year]=0;
            }
        }
        if(wonTeam){
            teamsWon[wonTeam][year]+=1;    
        }
    })
    return {
        obj:teamsWon,
        fileName:'matchesWonPerTeamPerYear'
    };
}




// Extra runs conceded per team in the year 2016

const runsConceded = function(matchData,deliveryData,YearToSearch = '2016'){
    
    let matchIdSeasonMap = {};
    matchData.forEach(match=>{
        id = match.id;
        season = match.season;
        matchIdSeasonMap[id]= season;
    })
    let deliveriesInYear = []
    for(let delivery of deliveryData){
        id = delivery.match_id;
        if(matchIdSeasonMap[id] === YearToSearch){
            deliveriesInYear.push(delivery);
        }
    }

    let extraRunsPerTeam = {};

    deliveriesInYear.forEach(delivery=>{

        team = delivery.bowling_team;
        extraRuns = delivery.extra_runs;

        if(team in extraRunsPerTeam){
            extraRunsPerTeam[team] += Number(extraRuns);
        }
        else{
            extraRunsPerTeam[team] = Number(extraRuns);
        }    
    });
    
     return {
         obj:extraRunsPerTeam,
         fileName:'extraRunsConcidedPerTeamIn2016'
    };
}

// Top 10 economical bowlers in the year 2015

const economicalBowlers = function(matchData,deliveryData,YearToSearch='2015'){
        
    let matchIdSeasonMap = {};
    matchData.forEach(match=>{
        id = match.id;
        season = match.season;
        matchIdSeasonMap[id]= season;
    })
    let deliveriesInYear = []
    for(let delivery of deliveryData){
        id = delivery.match_id
        if(matchIdSeasonMap[id] === YearToSearch){
            deliveriesInYear.push(delivery);
        }
    }

    let bowlerStats = {};
    let bowlerEconomy = {};

    deliveriesInYear.forEach(delivery=>{
        bowler = delivery.bowler;
        totalRuns = delivery.total_runs;
        wideBall=Number(delivery.wide_runs);
        noBall = Number(delivery.noball_runs);
        
        if(bowlerStats.hasOwnProperty(bowler)){

            if(typeof Number(totalRuns) !== 'number'){
                totalRuns = 0;
                bowlerStats[bowler].runs += Number(totalRuns);
            }
            else{
                bowlerStats[bowler].runs += Number(totalRuns);
            }
            if(!wideBall && !noBall){
                bowlerStats[bowler].balls += 1;
            }
        }
        else{
            if(typeof Number(totalRuns) != 'number'){
                totalRuns = 0;
                bowlerStats[bowler]={runs: Number(totalRuns)};
            }
            else{
                bowlerStats[bowler]={runs: Number(totalRuns)};
            }
            if(!wideBall && !noBall ){
                bowlerStats[bowler].balls =  1;
            }
            else{
                bowlerStats[bowler].balls =  0;
            }
        }
        runs = Number(bowlerStats[bowler].runs);
        balls = Number(bowlerStats[bowler].balls);
        economy = (runs*6)/balls;

        bowlerEconomy[bowler] = economy;
    });

    bowlerEconomy = Object.entries(bowlerEconomy);

    let top10Bowlers= bowlerEconomy.sort(function(a,b){
        if(a[1]>b[1]){
            return 1;
        }
        else if(a[1]<b[1]){
            return -1
        }
        else{
            return 0;
        }
    }).slice(0,10);

    top10Bowlers = Object.fromEntries(top10Bowlers);

    return {
        obj:top10Bowlers,
        fileName:'top10EconomicalBowlerIn2015'
    };
}

// Extra Problems

// number of times each team won the toss and also won the match
const tossAndMatchWon = function(matchData){
    let count = 0;
    result = {};
    for(let match of matchData){
        if(match.toss_winner === match.winner){  
            count++;
            result[match.toss_winner] = count;
        }
    }
    return {
        obj:result,
        fileName:'numberOfTossAndMatchWon'
    }
}


// player who has won the highest number of Player of the Match awards for each season
const playerOfTheMatch = function(matchData){
    let allplayers= {};
    matchData.forEach(match=>{
        let year = match.season;
        let playerName = match.player_of_match;

        if(!allplayers.hasOwnProperty(year)){
            allplayers[year] = {};        
        }
        if(!allplayers[year].hasOwnProperty(playerName)){
            allplayers[year][playerName] = 1;
        }
        else{
            allplayers[year][playerName] += 1;
        }
    })

    playerOfTheMatchPerSeason = {};
    for(let year in allplayers){
        
        playerOfTheMatchPerSeason[year]={};
        
        valueArray = Object.values(allplayers[year]);
        maxValue = Math.max(...valueArray);
        
        for(let player in allplayers[year]){
            if(allplayers[year][player] == maxValue){
                playerOfTheMatchPerSeason[year][player] = maxValue;
            }
        }
    }
    return {
        obj:playerOfTheMatchPerSeason,
        fileName:'mostPlayerOfTheMatchAwardWinnerPerSeason'
    };
}



//strike rate of a batsman for each season
const strikeRateOfAllBatsman = function(matchData,deliveryData){
    let allMatchData = {};
    let index = 0;
    for(let match of matchData){
          
        while(index< Number(deliveryData.length-1) && Number(deliveryData[index].match_id) === Number(match.id)){
            
            let matchSeason = match.season;
            let batsman = deliveryData[index].batsman;
            let batsmanRun = deliveryData[index].batsman_runs;
            let wideBall = Number(deliveryData[index].wide_runs);
            let noBall = Number(deliveryData[index].noball_runs);

            if(!allMatchData.hasOwnProperty(matchSeason)){
                allMatchData[matchSeason] = {};     
            }
            if(!allMatchData[matchSeason].hasOwnProperty(batsman)){
                allMatchData[matchSeason][batsman] = {};
                allMatchData[matchSeason][batsman].runs = Number(batsmanRun);
                if(!noBall && !wideBall){
                    allMatchData[matchSeason][batsman].balls = 1;
                }
                else{
                    allMatchData[matchSeason][batsman].balls = 0;
                }
            }
            else{
                
                allMatchData[matchSeason][batsman].runs += Number(batsmanRun);
                if(!noBall && !wideBall){
                    allMatchData[matchSeason][batsman].balls += 1;
                }
                let runs = allMatchData[matchSeason][batsman].runs;
                let balls = allMatchData[matchSeason][batsman].balls;
                let StrikeRate = (runs/balls)*100;

                allMatchData[matchSeason][batsman]['strikeRate'] = StrikeRate;
            }
            index++
        }
    }
    return {
        obj:allMatchData,
        fileName:'strikeRateOfPlayersPerSeason'
    }
}


// Highest number of times one player has been dismissed by another player

const playerDismissedByAnotherPlayer = function(deliveryData){
    let player = {}
    
    deliveryData.forEach(delivery=>{
        let batterDismissed = delivery.player_dismissed;
        let dismissedBy = delivery.bowler;
        if(batterDismissed){
            if(!player.hasOwnProperty(batterDismissed)){
                player[batterDismissed]={};
            }
            if(!player[batterDismissed].hasOwnProperty(dismissedBy)){
                player[batterDismissed][dismissedBy]=1;
            }
            else{
                player[batterDismissed][dismissedBy]+=1;
            }
        }
    })
    let higestTimeDismissedBY = {};

    for(let batsman in player){ 
        higestTimeDismissedBY[batsman] = {};
        maxTimeDismissed = Math.max(...Object.values(player[batsman]));
        
        for(let bowler in player[batsman]){
            
            let numberOfTimesDismissed = player[batsman][bowler];

            if(numberOfTimesDismissed == maxTimeDismissed){
                higestTimeDismissedBY[batsman][bowler]= numberOfTimesDismissed
            }
        }
    }
    return {
        obj:higestTimeDismissedBY,
        fileName:'playerDismissedAnotherPlayer'
    }

}


// Bowler with the best economy in super overs

const bestSuperOverBowler = function(deliveryData){
    let bowlerStats = {}
    let bowlerEconomy = {};
    deliveryData.forEach(delivery=>{
    if(delivery.is_super_over != 0){
        let superOverBowler = delivery.bowler;
        let totalRuns = Number(delivery.total_runs);
        let wideBall=Number(delivery.wide_runs);
        let noBall = Number(delivery.noball_runs);

            if(!bowlerStats.hasOwnProperty(superOverBowler)){
                bowlerStats[superOverBowler] = {runs:totalRuns};
                
                if(!wideBall && !noBall){
                    bowlerStats[superOverBowler].balls = 1;
                }
                else{
                    bowlerStats[superOverBowler].balls = 0;
                }
            }
            else{
                bowlerStats[superOverBowler].runs += totalRuns;
                if(!wideBall && !noBall){
                    bowlerStats[superOverBowler].balls += 1;
                }
            }
        let runsConceded = Number(bowlerStats[superOverBowler]['runs']);
        let ballsBowled = Number(bowlerStats[superOverBowler]['balls']);
        let economy = (runsConceded/ballsBowled)*6;
        bowlerEconomy[superOverBowler]= economy;
        }
    })

    lowestEconomy = Math.min(...Object.values(bowlerEconomy));
    lowestEconomyBowler = {};

    for(let bowler in bowlerEconomy){
        economy = bowlerEconomy[bowler];
        if(economy === lowestEconomy){
            lowestEconomyBowler[bowler] = economy;
        }
    }

    return {
        obj:lowestEconomyBowler,
        fileName:'bestSuperoverEconomyBowler'
    }
}





module.exports.matchPlayed = matchPlayed;
module.exports.matchesWonPerTeamPerYear = matchesWonPerTeamPerYear;
module.exports.runsConceded= runsConceded;
module.exports.economicalBowlers =economicalBowlers;


module.exports.tossAndMatchWon=tossAndMatchWon;
module.exports.playerOfTheMatch=playerOfTheMatch;
module.exports.strikeRateOfAllBatsman=strikeRateOfAllBatsman;
module.exports.playerDismissedByAnotherPlayer = playerDismissedByAnotherPlayer;
module.exports.bestSuperOverBowler=bestSuperOverBowler;
