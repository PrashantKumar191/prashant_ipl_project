const fs = require("fs");
const path = require('path');

const csv = require('csvtojson');
const ipl = require('./ipl');


const matchesCSVFilePath = path.resolve(`${__dirname}`,'../data/matches.csv');

function matchDataInput(functioninput){
    csv().fromFile(matchesCSVFilePath).then((jsonObj)=>{ 
        let result = functioninput(jsonObj);

        let outputFile = JSON.stringify(result.obj,null,2);
    
        let outputFilePath = path.resolve(`${__dirname}`,`../public/output/${result.fileName}.json`);
    
            fs.writeFile(outputFilePath,outputFile,function(err){
                if(err){
                    throw err;
                }
                console.log('saved')
            });   
    })
}

// Reading data from CSV and writing it to a json file.
matchDataInput(ipl.matchPlayed);
matchDataInput(ipl.matchesWonPerTeamPerYear);
matchDataInput(ipl.tossAndMatchWon);
matchDataInput(ipl.playerOfTheMatch);



const deliveriesCSVFilePath = path.resolve(`${__dirname}`,'../data/deliveries.csv');

function delivrieDataInput(functionInput){
    csv().fromFile(matchesCSVFilePath).then((jsonObj1)=>{
        csv().fromFile(deliveriesCSVFilePath).then((jsonObj2)=>{
            
            result = functionInput(jsonObj1,jsonObj2);
            
            let outputFile = JSON.stringify(result.obj,null,2);

            let outputFilePath = path.resolve(`${__dirname}`,`../public/output/${result.fileName}.json`);
        
            fs.writeFile(outputFilePath,outputFile,function(err){
                if(err){
                    throw err;
                }
                console.log('saved')
            });
        })
    })
}

// Reading data from CSV and writing it to a json file.
delivrieDataInput(ipl.runsConceded);
delivrieDataInput(ipl.economicalBowlers);
delivrieDataInput(ipl.strikeRateOfAllBatsman);



function onlyDeliverydataInput(functionInput){
    csv().fromFile(deliveriesCSVFilePath).then(jsonObj=>{
        let result = functionInput(jsonObj);

            let outputFile = JSON.stringify(result.obj,null,2);
        
            let outputFilePath = path.resolve(`${__dirname}`,`../public/output/${result.fileName}.json`);
        
                fs.writeFile(outputFilePath,outputFile,function(err){
                    if(err){
                        throw err;
                    }
                    console.log('saved')
                });
    })
}




onlyDeliverydataInput(ipl.playerDismissedByAnotherPlayer);
onlyDeliverydataInput(ipl.bestSuperOverBowler);